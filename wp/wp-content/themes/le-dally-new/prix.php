<?php
/*
 * Template name: TPL Prix
 */

/* BASE CONTEXT ALL PAGE INCLUDE */
$post = new TimberPost();
$context = Timber::get_context();
$detect = new Mobile_Detect();
$context['post'] = $post;
$context['charset'] = 'UTF-8';
$context['title'] = $post->post_title;
$context['content'] = $post->post_content;
$context['options'] = wp_load_alloptions();
$context['template_path'] = get_template_directory();
$context['template_path_uri'] = get_template_directory_uri();
$context['mobile_device'] = ($detect->isMobile() && !$detect->isTablet()) ? true : false;
$context['menus'] = wp_get_nav_menu_items('main');
$context['current_lang'] = qtranxf_getLanguage();
$context['languages'] = qtrans_getSortedLanguages();
/* / BASE CONTEXT ALL PAGE INCLUDE */

/* PAGE CONTEXT DATA */
$context['title_homepage'] = '[:fr]Une qualité de vie<br/>exceptionnelle[:en]An exceptional<br/>quality of life[:de]Eine aussergewöhnliche<br/>Lebensqualität';
$context['sliders'] = (have_rows('sliders')) ? get_field('sliders') : array();
$context['menu'] = new TimberMenu('home');


/* / PAGE CONTEXT DATA */
Timber::render( 'templates/header.html.twig' , $context );
Timber::render( 'templates/prix.html.twig' , $context );
Timber::render( 'templates/footer.html.twig' , $context );