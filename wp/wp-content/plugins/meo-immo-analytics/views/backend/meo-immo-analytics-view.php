<?php 
function meo_immo_analytics_admin_page(){ 
	
	global $immoOptions;
	
	// Populate data
	$data = array();
	
	foreach ($immoOptions as $immoOption => $optionDetail){
		$data[$immoOption] = get_option($immoOption);
	}
	
	// Form Validation
	if (isset($_POST['submit_immo_analytics'])){
		
		foreach ($immoOptions as $immoOption => $optionDetail){
			update_option($immoOption, $_POST[$immoOption]);
			// Over ride populated data
			$data[$immoOption] = $_POST[$immoOption];
		}
	}
?>

<form id="" name="" action="" method="post">
					
<div>
	<table class="form-table meo-form-table">
		<tr class="form-field">
			<td colspan="2"><h2>Smart Capture config &amp; PDF email details</h2></td>
		</tr>
		<tr class="form-field">
			<th><label>Piwik MEO Server</label></th>
			<td><em><?php echo MEO_ANALYTICS_SERVER; ?> (same as MEO Realestate)</em></td>
		</tr>
		<?php 
		// Prepare options
		foreach ($immoOptions as $immoOption => $optionDetail){
			echo '<tr class="form-field">
					<th><label>'.$optionDetail['label'].'</label></th>
					<td>';
					if($optionDetail['type'] == 'textarea'){
						wp_editor( stripslashes($data[$immoOption]), str_replace('_', '-', $immoOption), array('textarea_name' => $immoOption, 'textarea_rows' => 10, 'media_buttons' => false, 'teeny' => true) );
					}
					else echo '<input name="'.$immoOption.'" type="'.$optionDetail['type'].'" value="'.$data[$immoOption].'" />';
					
					if($optionDetail['notes'] != ''){
						echo '<br/><em>Notes: '.$optionDetail['notes'].'</em>';
					}
					
			echo '</td>
				</tr>';
		}
		?>
		<tr class="form-field">
			<td colspan="2"><input type="submit" name="submit_immo_analytics" value="save" /></td>
		</tr>
	</table>
</div>
					
</form>	

<?php 

}

?>