<?php
/*
 * Template name: TPL Homepage
 */

/* BASE CONTEXT ALL PAGE INCLUDE */
$post = new TimberPost();
$context = Timber::get_context();
$detect = new Mobile_Detect();
$context['post'] = $post;
$context['charset'] = 'UTF-8';
$context['site_url'] = get_site_url();
$context['title'] = $post->post_title;
$context['content'] = $post->post_content;
$context['options'] = wp_load_alloptions();
$context['template_path'] = get_template_directory();
$context['template_path_uri'] = get_template_directory_uri();
$context['mobile_device'] = ($detect->isMobile() && !$detect->isTablet()) ? true : false;
$context['menu'] = new TimberMenu('home');
$context['current_lang'] = qtranxf_getLanguage();
$context['languages'] = qtrans_getSortedLanguages();
/* / BASE CONTEXT ALL PAGE INCLUDE */

/* PAGE CONTEXT DATA */
$context['title_homepage'] = 'Une qualité de vie<br/>exceptionnelle';
$context['sliders'] = (have_rows('sliders')) ? get_field('sliders') : array();
/* / PAGE CONTEXT DATA */

Timber::render( 'templates/homepage.html.twig' , $context );