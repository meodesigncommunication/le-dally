/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function(){

    sliderInformations();

    $(window).resize(function(){
        sliderInformations();
    });

    var mySwiper = new Swiper ('.swiper-container', {
        loop: true,
        autoplay: {
            delay: 3000,
        },
    });

});

function sliderInformations() {
    var height = $('header.header').height()+60;

    if($('.slider-informations').width() == $(window).width()) {
        $('.slider-informations').css('top', height);
    }
}

