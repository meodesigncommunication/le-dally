<?php
/*
 * Template name: TPL Prix
 */

/* BASE CONTEXT ALL PAGE INCLUDE */
$post = new TimberPost();
$context = Timber::get_context();
$detect = new Mobile_Detect();
$context['post'] = $post;
$context['charset'] = 'UTF-8';
$context['class_body'] = 'page-content';
$context['title'] = $post->post_title;
$context['content'] = do_shortcode($post->post_content);
$context['options'] = wp_load_alloptions();
$context['template_path'] = get_template_directory();
$context['template_path_uri'] = get_template_directory_uri();
$context['mobile_device'] = ($detect->isMobile() && !$detect->isTablet()) ? true : false;
$wp_navigations = wp_get_nav_menu_items('home');

foreach($wp_navigations as $nav)
{
    if($nav->menu_item_parent != 0)
    {
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['title'] = $nav->title;
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['url'] = $nav->url;
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['object_id'] = $nav->object_id;
        if($post->ID == $nav->object_id) {
            $menus[$nav->menu_item_parent]['current'] = true;
            $menus[$nav->menu_item_parent]['childs'][$nav->ID]['current'] = true;
        }else{
            $menus[$nav->menu_item_parent]['childs'][$nav->ID]['current'] = false;
        }
    }else {
        $menus[$nav->ID]['title'] = $nav->title;
        $menus[$nav->ID]['url'] = $nav->url;
        $menus[$nav->ID]['object_id'] = $nav->object_id;
        if($post->ID == $nav->object_id) {
            $menus[$nav->ID]['current'] = true;
        }else{
            $menus[$nav->ID]['current'] = false;
        }
    }
}
$context['menus'] = $menus;
$context['meta_title'] = get_field('meta_title',$post->ID);
$context['meta_description'] = get_field('meta_description',$post->ID);
/* / BASE CONTEXT ALL PAGE INCLUDE */

Timber::render( 'templates/prix.html.twig' , $context );