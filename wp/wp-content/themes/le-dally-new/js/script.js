/**
 * Created by kylemobilia on 25.04.17.
 */
$(function(){
    $(document).ready(function(){
        $('.fancybox-gallery').fancybox();
        $('.fancybox').fancybox({
            'width': 415,
            'height': 800,
            'autoDimensions': false,
            'type': 'iframe',
            'autoSize': false
        });
    });
});
