$(document).ready(function(){
   
    //Nav switcher for mobile devices
    $('#nav-icon3').click(function(){
        //Toggle hamburger menu - btn css - nav open
        $(this).toggleClass('open');
        $(".phone-burger").toggleClass('open-switch');
        $("nav").toggleClass("open-menu");
    });

    $('.fancybox-smartcapture').fancybox({
        'width': 415,
        'height': 800,
        'autoDimensions': false,
        'type': 'iframe',
        'autoSize': false
    });

    $('.fancybox-gallery').fancybox({
        'width': 415,
        'height': 800,
        'autoDimensions': false,
        'type': 'iframe',
        'autoSize': false
    });  

});