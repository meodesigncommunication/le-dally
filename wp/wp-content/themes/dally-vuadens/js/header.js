$(function(){
    setbodyTopSpacing();

    $(window).scroll(function(){
        homeNavigation();
        scrollNavigation();
        setbodyTopSpacing();
    });
    $('i.fas.fa-bars').click(function(){
        mobileNavigation();
    });
    $('i.fas.fa-close').click(function(){
        mobileNavigation();
    });
});

function setbodyTopSpacing() {
    if($('body').hasClass('homepage')) {
        $('body').css('padding-top','0px');
    }else{
        $('body').css('padding-top',$('body .header').height()+90);
    }
}

function homeNavigation() {
    if($(window).scrollTop() <= $('.header').height()) {
        $('body.homepage .header').css('background-color','rgba(255,255,255,0.4)');
    }else{
        $('body.homepage .header').css('background-color','rgba(255,255,255,1)');
    }
}

function scrollNavigation() {
    if($(window).scrollTop() <= $('.header').height()) {
        $('.header a.logo img').css('max-width', '100%');
        $('.header').addClass('topScrolling');
    }else{
        $('.header a.logo img').css('max-width', '65%');
        $('.header').removeClass('topScrolling');
    }
}

function mobileNavigation() {
    if($('.header nav').css('visibility') == 'hidden') {
        $('.header nav').css('visibility', 'visible');
    }
    else {
        $('.header nav').css('visibility', 'hidden');
    }
}