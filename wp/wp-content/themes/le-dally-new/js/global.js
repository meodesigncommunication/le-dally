$(document).ready(function(){
    //Clone .home elems
    var oldWrap = $('.home').clone();
    $(".row-top-home, .row-bottom-home").contents().clone().appendTo('.swiper-wrapper');

    //Nav switcher for mobile devices
    $('#nav-icon3').click(function(){
        //Toggle hamburger menu - btn css - nav open
        $(this).toggleClass('open');
        $(".phone-burger").toggleClass('open-switch');
        $("nav").toggleClass("open-menu");
    });
});

//Call the slider
setTimeout(function(){
    $(".slider").find(".box-contact, .box-plans").remove();
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        autoplay: 2500
    });
}, 2000);